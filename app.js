const config = require('config');
const express = require('express');
const mongoose = require('mongoose');
const bluebird = require('bluebird');
const expressFileUpload = require('express-fileupload');

// libraries import
const respond = require('./libraries/respond');

// db start & configs
try {
    mongoose.Promise = bluebird;
    let isConnectedBefore = false;
    let connect = function () {
        mongoose.connect(config.get('db.connectionString'), {
            useNewUrlParser: true,
        });
    };
    connect();
    mongoose.connection.on('error', function () {
        console.log('Could not connect to MongoDB');
    });
    mongoose.connection.on('disconnected', function () {
        console.log('Lost MongoDB connection...');
        if (!isConnectedBefore) connect();
    });
    mongoose.connection.on('connected', function () {
        isConnectedBefore = true;
        console.log('Connection established to MongoDB');
    });
    mongoose.connection.on('reconnected', function () {
        console.log('Reconnected to MongoDB');
    });

    // Close the Mongoose connection, when receiving SIGINT
    process.on('SIGINT', function () {
        mongoose.connection.close(function () {
            console.log('Force to close the MongoDB conection');
            process.exit(0);
        });
    });
} catch (err) {
    throw err;
}

// router
const orderAPIRouter = require('./domains/order/v1/orderAPI');

// app
const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(expressFileUpload());

// api routes
app.use('/api/v1/orders', orderAPIRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    console.log("NOTFOUND");
    respond.responseNotFound(res);
});

// error handler
app.use(function (err, req, res, next) {
    console.log(err);
    respond.responseError(res);
});

// finalize
module.exports = app;
