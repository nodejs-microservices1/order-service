const Joi = require('joi');

const create = Joi.object({
    user_id: Joi.string().required(),
    items: Joi.array().items(Joi.object({
        menu_id: Joi.string().required(),
        quantity: Joi.number().min(1).required(),
    })).required(),
});

const updateStatus = Joi.object({
    status: Joi.string().allow('paid', 'delivered', 'cancelled').required(),
});

module.exports = {
    create,
    updateStatus,
};
