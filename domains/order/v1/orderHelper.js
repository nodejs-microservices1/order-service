const moment = require('moment');

const TAX_RATE = 0.11;

/**
 * Calculate the subtotal for an item
 * @param {Object} item
 */
const calculateItemSubtotal = (item) => {
    return Math.round(item.price - ((item.discount / 100) * item.price)) * item.quantity;
}

/**
 * Calculate the subtotal for an order
 * @param {Array} items
 */
const calculateSubtotal = (items) => {
    let subtotal = 0;
    for (let i = 0; i < items.length; i++) {
        subtotal += items[i].subtotal;
    }
    return subtotal;
}

/**
 * Calculate tax for an order
 * @param {Number} subtotal
 */
const calculateTax = (subtotal) => {
    return Math.round(subtotal * TAX_RATE);
}

/**
 * Calculate total for an order
 * @param {Number} subtotal
 * @param {Number} tax
 */
const calculateTotal = (subtotal, tax) => {
    return subtotal + tax;
}

/**
 * Generate Order Number
 */
const generateOrderNumber = () => {
    return `ORD-${moment().format('YYYYMMDDHHmmss')}`;
}

module.exports = {
    calculateItemSubtotal,
    calculateSubtotal,
    calculateTax,
    calculateTotal,
    generateOrderNumber
}
