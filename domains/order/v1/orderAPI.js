const express = require('express');
const orderController = require('./orderController');
const orderValidation = require('./orderValidation');
const validation = require('../../../middlewares/validation');

const router = express.Router();

router.get(
    '/',
    orderController.index
);

router.post(
    '/',
    validation(orderValidation.create),
    orderController.create
);

router.get(
    '/:id',
    orderController.detail
);

router.put(
    '/:id/status',
    validation(orderValidation.updateStatus),
    orderController.updateStatus
);

module.exports = router;
