const orderDAL = require('./orderDAL');
const orderHelper = require('./orderHelper');
const errorHelper = require('../../../libraries/error');
const mongoQuery = require('../../../libraries/mongoQuery');
const menuService = require('../../../libraries/internal-services/menu');
const userService = require('../../../libraries/internal-services/user');

/**
 * Get List Data
 * @param {Object} query values for filtering needs
 */
const index = async (query) => {
    // get query params
    const {page, limit, search, status, sort_by, sort_type} = query;

    // init filters
    let filters = [{}];

    // filter status
    if (status && status !== '') {
        filters.push({status: status});
    }

    // filter search
    if (search && search !== '') {
        filters.push({
            $or: [
                {order_number: mongoQuery.searchLike(search)},
            ],
        });
    }

    // sort
    const sort = mongoQuery.sortBy(sort_by, 'createdAt', sort_type, 'desc');

    // get data
    const orders = await orderDAL.list(filters, page, limit, sort);
    const totalFiltered = await orderDAL.total(filters);

    // response
    return {
        data: orders,
        meta: {
            total_filtered: totalFiltered.total,
        },
    };
};

/**
 * Create Data
 * @param {Object} body
 */
const create = async (body) => {
    // init new order data
    let newOrder = {
        status: "new"
    };

    // find, validation, and set user data
    const user = await userService.getUser(body.user_id);
    if (!user) errorHelper.throwUnprocessableEntity("User Not Found");
    newOrder.user = user._id;

    // find, validation, and set items data
    const bodyItems = body.items ? body.items : [];
    if (bodyItems.length === 0) errorHelper.throwUnprocessableEntity("Items Not Found");

    let items = [];
    for (let i = 0; i < bodyItems.length; i++) {
        const item = bodyItems[i];
        const menu = await menuService.getMenu(bodyItems[i].menu_id);
        if (!menu) errorHelper.throwUnprocessableEntity("Menu Not Found");

        item.menu = {
            id: menu._id.toString(),
            sku: menu.sku,
            name: menu.name,
        };
        item.quantity = bodyItems[i].quantity;
        item.price = menu.price;
        item.discount = menu.discount;
        item.subtotal = orderHelper.calculateItemSubtotal(item);

        items.push(item);
    }
    newOrder.items = items;

    // calculate subtotal
    newOrder.subtotal = orderHelper.calculateSubtotal(newOrder.items);

    // calculate tax
    newOrder.tax = orderHelper.calculateTax(newOrder.subtotal);

    // calculate total price
    newOrder.total = orderHelper.calculateTotal(newOrder.subtotal, newOrder.tax);

    // generate order number
    newOrder.order_number = orderHelper.generateOrderNumber();

    // create order
    let result = await orderDAL.create(newOrder);
    if (!result) errorHelper.throwInternalServerError("Create Failed");

    return result;
};

/**
 * Get Detail Data
 * @param {String} id
 */
const detail = async (id) => {
    const order = await orderDAL.findById(id);
    if (!order) errorHelper.throwNotFound();
    return order;
};

/**
 * Update Status
 * @param {String} id
 * @param {Object} body
 */
const updateStatus = async (id, body) => {
    const order = await orderDAL.findById(id);
    if (!order) errorHelper.throwNotFound();

    let result = await orderDAL.updateOne(id, body);
    if (!result) errorHelper.throwInternalServerError("Update Failed");

    return result;
};

module.exports = {
    index,
    create,
    detail,
    updateStatus,
};
