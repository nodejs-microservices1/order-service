const {Schema, model, SchemaTypes: {ObjectId}} = require('mongoose');

let itemSchema = new Schema({
    menu: {
        id: {
            type: String,
            required: true,
        },
        sku: {
            type: String,
            required: true,
        },
        name: {
            type: String,
            required: true,
        }
    },
    quantity: {
        type: Number,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
    discount: {
        type: Number,
        required: true,
    },
    subtotal: {
        type: Number,
        required: true,
    },
}, {_id: false})

let orderSchema = Schema(
    {
        user: {
            type: ObjectId,
            ref: "User",
            required: true,
        },
        order_number: {
            type: String,
            required: true,
            unique: true
        },
        items: [itemSchema],
        subtotal: {
            type: Number,
            required: true,
        },
        tax: {
            type: Number,
            required: true,
        },
        total: {
            type: Number,
            required: true,
        },
        status: {
            type: String,
            enum: ['new', 'paid', 'delivered', 'cancelled'],
            default: 'new',
            required: true,
        },
    },
    {timestamps: true}
);

exports.Order = model("Order", orderSchema);
exports.User = model("User", new Schema());
