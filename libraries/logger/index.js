const config = require('config');

const info = (message) => {
    if (config.get('node_env') === 'localhost') {
        console.log(message);
    } else {
        // use logger like logstash, etc
    }
};

module.exports = {info};
