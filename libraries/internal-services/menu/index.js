const axios = require('axios');
const config = require('config');
const logger = require('../../logger');

/**
 * Get Menu by menuID
 * @param {String} menuId
 * @returns
 */
const getMenu = async (menuId) => {
    try {
        const menuServiceUrl = config.get('internal_services.menu.url');
        const menuDetailUrl = `${menuServiceUrl}/api/v1/menus/${menuId}`;
        const response = await axios.request({
            method: "GET",
            url: menuDetailUrl,
        });

        let result;
        if (response && response.data && response.data.data) {
            result = response.data.data;
        }

        logger.info("====== menu_service getMenu : RESULT ======")
        logger.info(result)
        logger.info("====== menu_service getMenu : RESULT ======")

        return result;
    } catch (error) {
        logger.info("====== menu_service getMenu : ERROR ======")
        logger.info(error)
        logger.info("====== menu_service getMenu : ERROR ======")
        return null;
    }

};

module.exports = {
    getMenu,
};
