const axios = require('axios');
const config = require('config');
const logger = require('../../logger');

/**
 * Get User by userID
 * @param {String} userId
 * @returns
 */
const getUser = async (userId) => {
    try {
        const userServiceUrl = config.get('internal_services.user.url');
        const userDetailUrl = `${userServiceUrl}/api/v1/users/${userId}`;
        const response = await axios.request({
            method: "GET",
            url: userDetailUrl,
        });

        let result;
        if (response && response.data && response.data.data) {
            result = response.data.data;
        }

        logger.info("====== user_service getUser : RESULT ======")
        logger.info(result)
        logger.info("====== user_service getUser : RESULT ======")

        return result;
    } catch (error) {
        logger.info("====== user_service getUser : ERROR ======")
        logger.info(error)
        logger.info("====== user_service getUser : ERROR ======")
        return null;
    }

};

module.exports = {
    getUser,
};
